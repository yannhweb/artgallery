<?php

namespace App\Tests;

use App\Entity\Artwork;
use App\Entity\Category;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ArtworkUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $artwork = new Artwork();
        $datetime = new DateTime();
        $category = new Category();
        $user = new User();

        $artwork
            ->setName('peinture')
            ->setDescription('TrueDescription')
            ->setWidth(10)
            ->setHeight(10)
            ->setMadeAt($datetime)
            ->setUpdatedAt($datetime)
            ->setPortfolio(true)
            ->setSlug('peinture')
            ->setFile('peinture')
            ->addCategory($category)
            ->setUser($user);


        $this->assertTrue($artwork->getName() === 'peinture');
        $this->assertTrue($artwork->getDescription() === 'TrueDescription');
        $this->assertTrue($artwork->getWidth() == 10);
        $this->assertTrue($artwork->getHeight() == 10);
        $this->assertTrue($artwork->getMadeAt() === $datetime);
        $this->assertTrue($artwork->getUpdatedAt() === $datetime);
        $this->assertTrue($artwork->getPortfolio() === true);
        $this->assertTrue($artwork->getSlug() === 'peinture');
        $this->assertTrue($artwork->getFile() === 'peinture');
        $this->assertContains($category, $artwork->getCategory());
        $this->assertTrue($artwork->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $artwork = new Artwork();
        $datetime = new DateTime();
        $user = new User();

        $artwork
            ->setName('peinture')
            ->setDescription('TrueDescription')
            ->setWidth(10)
            ->setHeight(10)
            ->setMadeAt($datetime)
            ->setUpdatedAt($datetime)
            ->setPortfolio(true)
            ->setSlug('peinture')
            ->setFile('peinture')
            ->setUser($user);


        $this->assertFalse($artwork->getName() === 'Falsepeinture');
        $this->assertFalse($artwork->getDescription() === 'FalseDescription');
        $this->assertFalse($artwork->getWidth() == 20);
        $this->assertFalse($artwork->getHeight() == 20);
        $this->assertFalse($artwork->getMadeAt() === new DateTime());
        $this->assertFalse($artwork->getUpdatedAt() === new DateTime());
        $this->assertFalse($artwork->getPortfolio() === false);
        $this->assertFalse($artwork->getSlug() === 'Falsepeinture');
        $this->assertFalse($artwork->getFile() === 'Falsepeinture');
        $this->assertNotContains(new Category(), $artwork->getCategory());
        $this->assertFalse($artwork->getUser() === new User());
    }
}
