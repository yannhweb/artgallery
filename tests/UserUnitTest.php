<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();

        $user->setEmail('true@test.fr')
            ->setRoles(["ROLE_USER"])
            ->setPassword('password');

        $this->assertTrue($user->getEmail() === 'true@test.fr');
        $this->assertTrue($user->getpassword() === 'password');
        $this->assertTrue($user->getRoles() === ["ROLE_USER"]);
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail('true@test.fr')
            ->setRoles(["ROLE_USER"])
            ->setPassword('password');

        $this->assertFalse($user->getEmail() === 'false@test.fr');
        $this->assertFalse($user->getpassword() === 'falsepassword');
        $this->assertFalse($user->getRoles() === ["ROLE_ADMIN"]);
    }

}
