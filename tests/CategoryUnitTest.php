<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $category = new Category();

        $category
            ->setName('peinture')
            ->setDescription('peinture')
            ->setSlug('peinture');

        $this->assertTrue($category->getName() === 'peinture');
        $this->assertTrue($category->getDescription() === 'peinture');
        $this->assertTrue($category->getSlug() === 'peinture');
    }

    public function testIsFalse(): void
    {
        $category = new Category();

        $category
            ->setName('peinture')
            ->setDescription('peinture')
            ->setSlug('peinture');

        $this->assertFalse($category->getName() === 'Falsepeinture');
        $this->assertFalse($category->getDescription() === 'Falsepeinture');
        $this->assertFalse($category->getSlug() === 'Falsepeinture');
    }
}
