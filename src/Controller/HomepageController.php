<?php

namespace App\Controller;

use App\Repository\ArtworkRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/', name: 'homepage_')]
class HomepageController extends AbstractController
{
    #[Route('', name: 'main')]
    public function index(ArtworkRepository $artworkRepository): Response
    {
        return $this->render('homepage/index.html.twig', [
            'artworks' => $artworkRepository->findLastFive(),
        ]);
    }
}
