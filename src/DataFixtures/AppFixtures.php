<?php

namespace App\DataFixtures;

use App\Entity\Artwork;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $user = new User();
        $user->setEmail('demo@demo.fr')
            ->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $manager->persist($user);

        for ($i = 0; $i < 5; $i++) {
            $category = new Category();
            $category->setName($faker->word())
                ->setDescription((string)$faker->words(10, true))
                ->setSlug($faker->slug());
            $manager->persist($category);

            for ($j = 0; $j < 2; $j++) {
                $artwork = new Artwork();
                $artwork->setName((string)$faker->words(3, true))
                    ->setDescription((string)$faker->words(10, true))
                    ->setWidth($faker->randomFloat(2, 20, 60))
                    ->setHeight($faker->randomFloat(2, 20, 60))
                    ->setMadeAt($faker->dateTimeBetween('-2 years', 'now'))
                    ->setCreatedAt($faker->dateTimeBetween('-1 years', 'now'))
                    ->setUpdatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setPortfolio('true')
                    ->setFile($faker->file('public/img', 'public/img/upload', false))
                    ->setUser($user)
                    ->addCategory($category)
                    ->setSlug($faker->slug());

                $manager->persist($artwork);
            }
        }
        $manager->flush();
    }
}
